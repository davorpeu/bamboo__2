
document.addEventListener('DOMContentLoaded', function () {
  var splide = new Splide('.splide', {
    type: 'fade',
    rewind: true,
    perPage: 1,
    cover: true,
    //alot of this doesnt work
    heightRatio: 0.5,
    width: '90vw',
    height: '30vh',
    fixedHeight: '50vh',
    breakpoints: {
      375: {
        perPage: 3,
        gap: '.7rem',
        height: '40rem',
        width: '30vh',

      },
    },
  });

  splide.mount();
});

function openNav() {
  let x = document.querySelector(".navigation__links");
  x.style.display = "block";
  x.style.opacity = "90%";
  x.style.width = "100%";
}
function closeNav() {
  let x = document.querySelector(".navigation__links");
  x.style.width = "0%";
  x.style.opacity = "0%";

}